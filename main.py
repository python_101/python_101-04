# 6.2 Write Your Own Functions
# 1. Write a function called cube() with one number parameter and returns the value of that number raised to the third
# power. Test the function by displaying the result of calling your cube() function on a few different numbers.
def cube(number):
    result = number ** 3
    return result


num = 3
print(cube(num))
# 2. Write a function called greet() that takes one string parameter called name and displays the text "Hello <name>!",
# where <name> is replaced with the value of the name parameter.


def greet(name):
    print(f"Hello {name}")
    return


question = input("What's your name? ")
greet(question)

# 6.3 Challenge: Convert Temperatures
# Write a script called temperature.py that defines two functions:
# 1. convert_cel_to_far() which takes one float parameter representing degrees Celsius and returns a float
# representing the same temperature in degrees Fahrenheit using the following formula:
# F = C * 9/5 + 32
# 2. convert_far_to_cel() which take one float parameter representing degrees Fahrenheit and returns a float
# representing the same temperature in degrees Celsius using the following formula:
# C = (F - 32) * 5/9
# The script should first prompt the user to enter a temperature in degrees Fahrenheit and then display the temperature
# converted to Celsius. Then prompt the user to enter a temperature in degrees Celsius and display the temperature
# converted to Fahrenheit. All converted temperatures should be rounded to 2 decimal places.
# Here’s a sample run of the program:
# Enter a temperature in degrees F: 72
# 72 degrees F = 22.22 degrees C
# Enter a temperature in degrees C: 37
# 37 degrees C = 98.60 degrees F


def convert_cel_to_far(c):
    f = c * 9/5 + 32
    return f


temperature = float(input("Enter a temperature in degrees C: "))
result = convert_cel_to_far(temperature)
print(f"{temperature:.0f} degrees C = {result:.2f} degrees F")


def convert_far_to_cel(f):
    c = (f - 32) * 5/9
    return c


temperature = float(input("Enter a temperature in degrees F: "))
result = convert_far_to_cel(temperature)
print(f"{temperature:.0f} degrees F = {result:.2f} degrees C")
# 6.4 Run in Circles
# 1. Write a for loop that prints out the integers 2 through 10, each on a new line, by using the range() function.
for i in range(2, 11):
    print(i)
# 2. Use a while loop that prints out the integers 2 through 10 (Hint: You’ll need to create a new integer first.)
num = 2
while num < 11:
    print(num)
    num = num + 1

# 3. Write a function called doubles() that takes one number as its input and doubles that number. Then use the
# doubles() function in a loop to double the number 2 three times, displaying each result on a separate line.
# Here is some sample output:
# 4
# 8
# 16


def doubles(num):
    return num * 2


my_num = 2
for i in range(1, 4):
    my_num = doubles(my_num)
    print(my_num)

# 6.5 Challenge: Track Your Investments
# In this challenge, you will write a program called invest.py that tracks the growing amount of an investment over time
# An initial deposit, called the principal amount, is made. Each year, the amount increases by a fixed percentage,
# called the annual rate of return.
# For example, a principal amount of $100 with an annual rate of return of 5% increases the first year by $5. The second
# year, the increase is 5% of the new amount $105, which is $5.25. Write a function called invest with three parameters:
# the principal amount, the annual rate of return, and the number of years to calculate. The function signature might
# look something like this:
# def invest(amount, rate, years):
# The function then prints out the amount of the investment, rounded to 2 decimal places, at the end of each year for
# the specified number of years.
# For example, calling invest(100, .05, 4) should print the following:
# year 1: $105.00
# year 2: $110.25
# year 3: $115.76
# year 4: $121.55
# To finish the program, prompt the user to enter an initial amount, an annual percentage rate, and a number of years.
# Then call invest() to display the calculations for the values entered by the user.


def invest(amount, rate, years):
    for year in range(1, years+1):
        amount = amount + amount * rate
        print(f"Year {year}: ${amount:.2f}")
    return


invest(100, .05, 4)
